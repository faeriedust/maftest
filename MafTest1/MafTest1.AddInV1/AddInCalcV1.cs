﻿using System;
using System.AddIn;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MafTest1.AddInView;

namespace MafTest1.AddInV1 {
  [AddIn("Calculator AddIn", Version = "1.0.0.0")]
  public class AddInCalcV1 : ICalculator {
    public double Add(double a, double b) {
      return a + b;
    }
    public double Subtract(double a, double b) {
      return a - b;
    }
    public double Multiply(double a, double b) {
      return a * b;
    }
    public double Divide(double a, double b) {
      return a / b;
    }
  }
}
