﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MafTest1.AddInView;
using MafTest1.Contract;
using System.AddIn.Pipeline;

namespace MafTest1.AddInSideAdapter {
  [AddInAdapter]
  public class CalculatorViewToContractAddInSideAdapter : ContractBase, ICalculatorContractV1 {
    public CalculatorViewToContractAddInSideAdapter(ICalculator calculator) {
      _View = calculator;
    }
    private ICalculator _View;
    public double Add(double a, double b) {
      return _View.Add(a, b);
    }
    public double Subtract(double a, double b) {
      return _View.Subtract(a, b);
    }
    public double Multiply(double a, double b) {
      return _View.Multiply(a, b);
    }
    public double Divide(double a, double b) {
      return _View.Divide(a, b);
    }
  }
}
