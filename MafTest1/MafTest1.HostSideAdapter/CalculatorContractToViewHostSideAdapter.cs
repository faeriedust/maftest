﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn.Pipeline;
using MafTest1.Contract;
using MafTest1.HVA;

namespace MafTest1.HostSideAdapter {
  [HostAdapter]
  public class CalculatorContractToViewHostSideAdapter : ICalculator {
    public CalculatorContractToViewHostSideAdapter(ICalculatorContractV1 contract) {
      _Contract = contract;
      _Handle = new ContractHandle(contract);
    }
    private ICalculatorContractV1 _Contract;
    /// <remarks>
    /// This object handles lifetime management.
    /// Don't lose this reference or it'll get garbage collected and the pipeline will die.
    /// </remarks>
    private ContractHandle _Handle;
    public double Add(double a, double b) {
      return _Contract.Add(a, b);
    }
    public double Subtract(double a, double b) {
      return _Contract.Subtract(a, b);
    }
    public double Multiply(double a, double b) {
      return _Contract.Multiply(a, b);
    }
    public double Divide(double a, double b) {
      return _Contract.Divide(a, b);
    }
  }
}
